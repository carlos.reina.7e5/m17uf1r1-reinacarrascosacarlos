using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private Button _button;
    public Text NameInput;
    public static string CharacterName;
    public static string GameMode;

    // Start is called before the first frame update
    void Start()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(LoadScene);
    }

    void LoadScene()
    {
        switch (_button.GetComponentInChildren<Text>().text)
        {
            case "MODE 1":
                SceneManager.LoadScene("Game1Scene");
                GameMode = "Game1Scene";
                break;
            case "MODE 2":
                SceneManager.LoadScene("Game2Scene");
                GameMode = "Game2Scene";
                break;
            case "PLAY AGAIN":
                SceneManager.LoadScene(GameMode);
                break;
            case "MAIN MENU":
                SceneManager.LoadScene("StartScreen");
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (SceneManager.GetActiveScene().name == "StartScreen") CharacterName = NameInput.text;
    }
}
