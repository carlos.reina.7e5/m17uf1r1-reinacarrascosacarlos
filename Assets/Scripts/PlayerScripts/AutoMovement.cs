using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoMovement : MonoBehaviour
{

    public float Speed;
    private float _minX = -7.65f;
    private float _maxX = 7.65f;
    private bool _facingRight = true;
    private bool _facingLeft = false;
    private Animator _animator;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _animator.SetBool("isRunning", true);
    }

    // Update is called once per frame
    void Update()
    {
        CheckDir();
        if (_facingRight) transform.Translate(Vector3.right * Time.deltaTime * Speed, 0);
        if (_facingLeft) transform.Translate(Vector3.left * Time.deltaTime * Speed, 0);
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, _minX, _maxX), transform.position.y, transform.position.z);
    }

    void CheckDir()
    {
        if (transform.position.x == 7.65f)
        {
            Flip();
            _facingRight = false;
            _facingLeft = true;
        }

        if (transform.position.x == -7.65f)
        {
            Flip();
            _facingRight = true;
            _facingLeft = false;
        }
    }

    void Flip()
    {
        Vector3 currentScale = gameObject.transform.localScale;
        currentScale.x *= -1;
        gameObject.transform.localScale = currentScale;

        _facingRight = !_facingRight;
    }
}
