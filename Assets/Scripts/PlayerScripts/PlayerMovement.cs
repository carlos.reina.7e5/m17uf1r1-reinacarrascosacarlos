using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float SpeedModifier;
    private Rigidbody2D _rb;
    private float moveInput;
    public float JumpForce;
    private bool isGrounded;
    public Transform FeetPos;
    public float CheckRadius;
    public LayerMask WhatIsGround;
    private float _jumpTimeCounter;
    public float JumpTime;
    private bool _isJumping;
    private Animator _animator;
    private float _minX = -10.57f;
    private float _maxX = 10.57f;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _rb = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics2D.OverlapCircle(FeetPos.position, CheckRadius, WhatIsGround);

        if (moveInput > 0)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
        else if (moveInput < 0)
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }

        if(isGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            _animator.SetTrigger("takeOff");
            _isJumping = true;
            _jumpTimeCounter = JumpTime;
            _rb.velocity = Vector2.up * JumpForce;
        }

        if (isGrounded)
        {
            _animator.SetBool("isJumping", false);
        }
        else
        {
            _animator.SetBool("isJumping", true);
        }

        if (Input.GetKey(KeyCode.Space) && _isJumping)
        {
            if(_jumpTimeCounter > 0)
            {
                _rb.velocity = Vector2.up * JumpForce;
                _jumpTimeCounter -= Time.deltaTime;
            } 
            else
            {
                _isJumping = false;
            }
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            _isJumping = false;
        }

        if (moveInput == 0)
        {
            _animator.SetBool("isRunning", false);
        }
        else
        {
            _animator.SetBool("isRunning", true);
        }
    }

    void FixedUpdate()
    {
        moveInput = Input.GetAxisRaw("Horizontal");
        _rb.velocity = new Vector2(moveInput * SpeedModifier, _rb.velocity.y);
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, _minX, _maxX), transform.position.y, transform.position.z);
    }
}
