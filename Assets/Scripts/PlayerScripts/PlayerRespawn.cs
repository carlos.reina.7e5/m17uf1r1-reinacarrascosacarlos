using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerRespawn : MonoBehaviour
{
    public void PlayerHit()
    {
        SceneManager.LoadScene("GameOverScene");
    }

    private void OnBecameInvisible()
    {
        SceneManager.LoadScene("GameOverScene");
    }
}
