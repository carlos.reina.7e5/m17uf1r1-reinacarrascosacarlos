using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetPlayerResult : MonoBehaviour
{

    private Text _txt;
    private bool _result;
    private string _gameMode;

    // Start is called before the first frame update
    void Start()
    {
        _txt = GetComponent<Text>();
        _result = GameManager.Victory;
        _gameMode = UIManager.GameMode;
    }

    // Update is called once per frame
    void Update()
    {
        if (_gameMode == "Game1Scene")
        {
            if (_result) _txt.text = "Congratulations " + UIManager.CharacterName + "!";
            else _txt.text = "Better luck next time, " + UIManager.CharacterName;
        }
        else
        {
            if (_result) _txt.text = "Better luck next time, " + UIManager.CharacterName;
            else _txt.text = "Congratulations " + UIManager.CharacterName + "!";
        }
    }
}
