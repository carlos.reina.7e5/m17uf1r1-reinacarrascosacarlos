using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputSpawn : MonoBehaviour
{
    public GameObject FireBall;
    public Transform SpawnRangeRight;
    public Transform SpawnRangeLeft;
    public Transform SpawnRangeUp;
    public Transform SpawnRangeDown;

    // Start is called before the first frame update
    void Update()
    {
        if (Input.GetMouseButtonDown(0)) SpawnObjects();
    }

    void SpawnObjects()
    {
        Vector3 spawnPos = new Vector3(0, 0, 0);

        spawnPos = new Vector3(Random.Range(SpawnRangeLeft.position.x, SpawnRangeRight.position.x), Random.Range(SpawnRangeDown.position.y, SpawnRangeUp.position.y), 0);

        GameObject obj = Instantiate(FireBall, spawnPos, gameObject.transform.rotation);
    }
}
