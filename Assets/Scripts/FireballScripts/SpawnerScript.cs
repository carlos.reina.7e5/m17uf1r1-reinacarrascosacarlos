using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerScript : MonoBehaviour
{
    public GameObject FireBall;
    public Transform SpawnRangeRight;
    public Transform SpawnRangeLeft;
    public Transform SpawnRangeUp;
    public Transform SpawnRangeDown;
    public float TimeSpawn;
    public float TimeSpawnDelay;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnObjects", TimeSpawn, TimeSpawnDelay);
    }

    void SpawnObjects()
    {
        Vector3 spawnPos = new Vector3(0, 0, 0);

        spawnPos = new Vector3(Random.Range(SpawnRangeLeft.position.x, SpawnRangeRight.position.x), Random.Range(SpawnRangeDown.position.y, SpawnRangeUp.position.y), 0);

        GameObject obj = Instantiate(FireBall, spawnPos, gameObject.transform.rotation);
    }
}
