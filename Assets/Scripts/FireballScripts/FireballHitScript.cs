using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballHitScript : MonoBehaviour
{

    private Animator _animator;
    private Rigidbody2D _rb;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _rb = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            collision.transform.GetComponent<PlayerRespawn>().PlayerHit();
        }

        if (collision.transform.CompareTag("Ground"))
        {
            _rb.velocity = Vector3.zero;
            _rb.gravityScale = 0;
            _animator.SetTrigger("collision");
            Destroy(gameObject, 0.3f);
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

}
