# REPTE 1

## RESUM:
### Dos modes de joc, als dos hi ha un terreny amb possibilitats de caure, un personatge i boles de foc que cauen.  
### MODE 1
#### Al primer mode de joc, el jugador controla al personatge i ha d'esquivar les boles de foc. Si toca una bola o cau del terreny, mor. Si es manté viu durant una determinada quantitat de temps, guanya. 
### MODE 2
#### Al segon mode de joc, el jugador controla la posició de generació de les boles de foc. Haurà de colpejar un personatge que es mourà automàticament. El jugador guanyarà quan aconsegueixi que una bola de foc impacti amb el personatge, i perdrà si després d'un determinat temps no ha aconseguit colpejar-li.

## TO DO (de moment només estan els punts obligatoris, no hi ha cap bonus):  
- [x] **Mode 1**
	- [x] Terreny
		- [x] Background (pel moment hi ha un provisional)
		- [x] Plataforma (un objecte diferent del background, s'ha de poder caure, per tant ha de ser menys llarga que la pantalla)
			- [x] Colisions (sense RigidBody per que no li afectin les físiques)
	- [x] Personatge (jugador)
		- [x] Moviment
		- [x] Salt
		- [x] Colisions (amb RigidBody per les físiques)
		- [ ] Animacions
			- [x] Idle
			- [x] Moviment
			- [x] Salt (amb preparació i caiguda)
			- [~] Mort (amb destrucció)
		- [x] Trigger bola de foc (derrota)
		- [x] Trigger al caure de la plataforma (derrota)
		- [x] Trigger victòria (comptador arriba a 0 i el jugador continua amb vida)
	- [x] Bola de foc (automàtica)
		- [x] Creació aleatoria (spawn)
		- [x] Caiguda en línea recta (gravetat del RigidBody)
		- [x] Animacions
			- [x] Caiguda
			- [x] Destrucció
		- [x] Colisions (suposo que amb RigidBody per les físiques, pero s'ha de veure)
		- [x] Destrucció
		- [x] Trigger personatge
	- [x] GameManager
- [x] **Mode 2**
	- [x] Terreny
		- [x] Background
		- [x] Colisions (sense RigidBody per que no li afectin les físiques)
	- [ ] Personatge (automàtic)
		- [x] Moviment automàtic
		- [~] IA? (per que intenti esquivar les boles i no es mogui com un NPC..?) (bonus, pel moment no importa)
		- [x] Colisions (amb RigidBody per les físiques)
		- [x] Animacions
			- [~] Idle
			- [x] Moviment
			- [~] Mort (amb destrucció)
		- [x] Trigger bola de foc
	- [ ] Bola de foc (jugador)
		- [x] Creació manual via input d'usuari (click esquerra)
		- [~] Punter? Fletxa? Indicador visual per veure on spawnearà la bola al pulsar el botó...
		- [x] Animacions
			- [x] Caiguda
			- [x] Destrucció
		- [x] Colisions (suposo que amb RigidBody per les físiques, pero s'ha de veure)
		- [x] Destrucció
		- [x] Trigger personatge (victòria)
		- [x] Trigger derrota (comptador arriba a 0 i el personatge continua amb vida)
	- [x] GameManager
- [x] **Hud** (puc possar més coses, però de moment això és lo essencial)
	- [~] Icona presonatge
	- [~] Nom
	- [~] Barra de vida? (no necessari si mor d'un hit)
	- [x] Temporitzador (teoricament els dos modes el necessiten)
- [x] **Pantalles**
	- [x] StartScreen (puc possar més coses, però de moment això és lo essencial)
		- [x] Títol (de moment no l'he decidit)
		- [x] InputField pel nom del personatge
		- [x] Botons dels modes
			- [x] Botó mode 1
			- [x] Botó mode 2
	- [x] GameOverScreen
		- [x] Text anunciant Victòria/Derrota amb el nom del personatge (ex: victòria del guerrer Lancelot..)
		- [x] Botó de Play Again
		- [x] Botó de Main Menu